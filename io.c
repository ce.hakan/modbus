/*
 *
 */

#include <stdio.h>
#include <fcntl.h>
#include <linux/i2c-dev.h>
#include <errno.h>

#include "io.h"

#define I2C_ADDR    0x38    //output
#define I2C_ADDR2   0x3e    //input

int read_INPUT(char io, unsigned char pin, unsigned char *data) {
    char buffer[1];
    int fd;
    char cmp;

    fd = open("/dev/i2c-0", O_RDWR);
    
    if(fd < 0) {
        printf("Error openning file: %s\n", strerror(errno));
        return 1;
    }

    if(io == 1) {
        if(ioctl(fd, I2C_SLAVE, I2C_ADDR) < 0) {
            printf("ioctl error: %s\n", strerror(errno));
            return 1;
        }
    }
    else {
        if(ioctl(fd, I2C_SLAVE, I2C_ADDR2) < 0) {
            printf("ioctl error: %s\n", strerror(errno));
            return 1;
        }
    }

    if(read(fd, &buffer[0], 1) != 1) {
        printf("read error: %s\n", strerror(errno));
        return 1;
    }

    close(fd);

    switch (pin)
    {
    case '0':
        pin = 0x01;
        break;
    case '1':
        pin = 0x02;
        break;
    case '2':
        pin = 0x04;
        break;
    case '3':
        pin = 0x08;
        break;
    case '4':
        pin = 0x01;
        break;
    case '5':
        pin = 0x02;
        break;
    case '6':
        pin = 0x04;
        break;
    case '7':
        pin = 0x08;
        break;
    case '8':
        pin = 0x10;
        break;
    case '9':
        pin = 0x20;
        break;
    case 'A':
        pin = 0x40;
        break;
    case 'B':
        pin = 0x80;
        break;
    default:
        break;
    }

    cmp = (0xFF & buffer[0]) & pin;

    if(cmp >= 1) *data = 1;
    else *data = 0;

    printf("Input: 0x%02x pin: 0x%02x data: %d\n", buffer[0], pin, *data);

    return 0;
}

int write_OUTPUT(unsigned char pin, unsigned char data) {
    char buffer[1];
    int fd;
    char cmp[1];

    fd = open("/dev/i2c-0", O_RDWR);

    if(fd < 0) {
        printf("Error openning file: %s\n", strerror(errno));
        return 1;
    }

    if(ioctl(fd, I2C_SLAVE, I2C_ADDR) < 0) {
        printf("ioctl error: %s\n", strerror(errno));
        return 1;
    }

    if(read(fd, &buffer[0], 1) != 1) {
        printf("read from reagister");
        return 1;
    }

    close(fd);

    switch (pin)
    {
    case '4':
        pin = 0x01;
        break;
    case '5':
        pin = 0x02;
        break;
    case '6':
        pin = 0x04;
        break;
    case '7':
        pin = 0x08;
        break;
    case '8':
        pin = 0x10;
        break;
    case '9':
        pin = 0x20;
        break;
    case 'A':
        pin = 0x40;
        break;
    case 'B':
        pin = 0x80;
        break;
    default:
        break;
    }

    if(data == '1') cmp[0] = (buffer[0] | (pin));
    else if(data == '0') cmp[0] = (buffer[0] & (~pin));

    fd = open("/dev/i2c-0", O_RDWR);

    if(fd < 0) {
        printf("Error openning file: %s\n", strerror(errno));
        return 1;
    }

    if(ioctl(fd, I2C_SLAVE, I2C_ADDR) < 0) {
        printf("ioctl error: %s\n", strerror(errno));
        return 1;
    }

    if(write(fd, &cmp[0], 1) != 1) {
        printf("write to register");
        return 1;
    }

    close(fd);

    printf("Output: 0x%02x pin: 0x%02x out: 0x%02x\n", buffer[0], pin, cmp[0]);

    return 0;
}
