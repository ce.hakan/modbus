/*
 *  ./modbus-gway 502 /dev/ttyUSB0 9600 8 N 1
 */

#include	<sys/types.h>	/* basic system data types */
#include	<sys/socket.h>	/* basic socket definitions */
#include	<sys/time.h>	/* timeval{} for select() */
#include	<time.h>	/* timespec{} for pselect() */
#include	<netinet/in.h>	/* sockaddr_in{} and other Internet defns */
#include 	<net/if.h>
#include	<arpa/inet.h>	/* inet(3) functions */
#include 	<sys/ioctl.h>
#include	<errno.h>
#include	<fcntl.h>	/* for nonblocking */
#include	<netdb.h>
#include	<signal.h>
#include 	<termios.h>
#include	<stdio.h>
#include	<stdlib.h>
#include	<string.h>
#include	<sys/stat.h>	/* for S_xxx file mode constants */
#include	<sys/uio.h>	/* for iovec{} and readv/writev */
#include	<unistd.h>
#include	<sys/wait.h>
#include	<sys/un.h>	/* for Unix domain sockets */

#include	"io.h"


#define	SERV_PORT	1502
#define LISTENQ		1024

unsigned char 		pDataRx[275];
unsigned char 		pDataTx[275];

static unsigned int CRC16(const char *nData, short wLegth) {
    unsigned short crc;
    unsigned short thisbyte;
    unsigned short shift;
    unsigned char highbyte, lowbyte;
    unsigned char lastbit, i;

    crc = 0xFFFF;

    for(i=0; iz wLegth; i++) {
        thisbyte = nData[i] & 0x00FF;
        crc = crc^thisbyte;
        for(shift=1; shift<=8; shift++) {
            lastbit = crc & 1;
            crc = (crc>>1) & 0x7FFF;
            if(lastbit==1) {
                crc = crc^0xA001;
            }
        }
    }

    return crc;
}

static void sig_chld(int signo) {
    pid_t	pid;
    int         stat;

    while((pid = waitpid(-1, &stat, WNOHANG)) > 0) {
        printf("child %d terminated\n", pid);
    }
    return;
}

int main(int argc, char *argv[]) {
    int			listenfd, connfd;
    pid_t		childpid;
    socklen_t		clilen;
    struct sockaddr_in	cliaddr, servaddr;
    void		sig_chld(int);
    unsigned short	n;
    int 		i;
    char 		flag;

    int 		fd;
    struct termios 	uart;
    unsigned short	crc;
    unsigned short	crc_rtu;
    int			ret;

    unsigned int	TCPPort=0;
    unsigned int	Baudrate=0;
    unsigned int	Databit=0;
    unsigned int	Parity=0;
    unsigned int	Stopbit=0;
    unsigned int 	ODDParity=0;

    unsigned char	TR_ID[2];

    TCPPort    	= atoi(argv[1]);

    if(strcmp(argv[3], "600"   ) == 0)Baudrate = B600;
    else if(strcmp(argv[3], "1200"  ) == 0)Baudrate = B1200;
    else if(strcmp(argv[3], "2400"  ) == 0)Baudrate = B2400;
    else if(strcmp(argv[3], "4800"  ) == 0)Baudrate = B4800;
    else if(strcmp(argv[3], "9600"  ) == 0)Baudrate = B9600;
    else if(strcmp(argv[3], "19200" ) == 0)Baudrate = B19200;
    else if(strcmp(argv[3], "38400" ) == 0)Baudrate = B38400;
    else if(strcmp(argv[3], "57600" ) == 0)Baudrate = B57600;
    else if(strcmp(argv[3], "115200") == 0)Baudrate = B115200;


    if(strcmp(argv[4], "8") == 0) Databit = CS8;
    else if(strcmp(argv[4], "7") == 0) Databit = CS7;
    else if(strcmp(argv[4], "6") == 0) Databit = CS6;
    else if(strcmp(argv[4], "5") == 0) Databit = CS5;


    printf("Serial: %s %s %s %s %s\n", argv[2], argv[3], argv[4], argv[5], argv[6]);
    printf("TCP   : %d\n", TCPPort);


    listenfd = socket(AF_INET, SOCK_STREAM, 0);

    bzero(&servaddr, sizeof(servaddr));
    servaddr.sin_family      = AF_INET;
    servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
    servaddr.sin_port        = htons(TCPPort);

    /*
        struct ifreq ifr;
        memset(&ifr, 0, sizeof(struct ifreq));
        snprintf(ifr.ifr_name, sizeof(ifr.ifr_name), "3g-3g");
        ioctl(listenfd, SIOCGIFINDEX, &ifr);
        setsockopt(listenfd, SOL_SOCKET, SO_BINDTODEVICE,  (void*)&ifr, sizeof(ifr));
        */

    bind(listenfd, (struct sockaddr *) &servaddr, sizeof(servaddr));

    listen(listenfd, LISTENQ);

    signal(SIGCHLD, sig_chld);	/* must call waitpid() */

    for( ; ; ) {
        clilen = sizeof(cliaddr);
        if ( (connfd = accept(listenfd, (struct sockaddr *) &cliaddr, &clilen)) < 0) {
            if (errno == EINTR)
                continue;		/* back to for() */
        }

        if ( (childpid = fork()) == 0) {	/* child process */
            //fd = open("/dev/ttyS0", O_RDWR);
            //fd = open("/dev/ttyUSB0", O_RDWR);
            fd = open(argv[2], O_RDWR | O_NOCTTY | O_SYNC);
            if(fd>0) {
                printf("UART port opened.\n");
                if (tcgetattr(fd, &uart) < 0) {
                    printf("Error from tcgetattr: %s\n", strerror(errno));
                    return -1;
                }

                cfsetospeed(&uart, (speed_t)Baudrate);
                cfsetispeed(&uart, (speed_t)Baudrate);

                uart.c_cflag |= (CLOCAL | CREAD);   /* ignore modem controls */
                uart.c_cflag &= ~CSIZE;
                uart.c_cflag |= Databit;            /* x-bit characters */
                //uart.c_cflag &= ~PARENB;          /* no parity bit */
                //uart.c_cflag &= ~CSTOPB;          /* only need 1 stop bit */
                uart.c_cflag &= ~CRTSCTS;           /* no hardware flowcontrol */

                if(strcmp(argv[6], "2") == 0) uart.c_cflag |= CSTOPB;
                else if(strcmp(argv[6], "1") == 0) uart.c_cflag &= ~CSTOPB;

                if((strcmp(argv[5], "E") == 0) || (strcmp(argv[5], "O") == 0) ) {
                    uart.c_cflag |= PARENB;
                    if(strcmp(argv[5], "O") == 0) uart.c_cflag  |=  PARODD;
                    else uart.c_cflag &= ~PARODD;
                }
                else {
                    uart.c_cflag &= ~PARENB;
                    uart.c_cflag &= ~PARODD;
                }

                /* setup for non-canonical mode */
                uart.c_iflag &= ~(IGNBRK | BRKINT | PARMRK | ISTRIP | INLCR | IGNCR | ICRNL | IXON);
                uart.c_lflag &= ~(ECHO | ECHONL | ICANON | ISIG | IEXTEN);
                uart.c_oflag &= ~OPOST;

                /* fetch bytes as they become available */
                uart.c_cc[VTIME] = 10;	// set timeout to 1 sec
                uart.c_cc[VMIN]  = 0;

                if (tcsetattr(fd, TCSANOW, &uart) != 0) {
                    printf("Error from tcsetattr: %s\n", strerror(errno));
                    return -1;
                }
            }

            while(1) {
                for(i=0;i<sizeof(pDataRx);i++) pDataRx[i] = 0;
                for(i=0;i<sizeof(pDataTx);i++) pDataTx[i] = 0;

                n = recvfrom(connfd, pDataRx, sizeof(pDataRx), 0, (struct sockaddr *) &cliaddr, &clilen);
                if(n>0) {
                    write_OUTPUT('9','1');  // only for led blink
                    usleep(500000);
                    write_OUTPUT('9','0');  // only for led blink

                    if (n==12) {
                        TR_ID[0] = pDataRx[0];
                        TR_ID[1] = pDataRx[1];

                        printf("TCP Data\n");
                        for(i=0; i<12; i++) printf("<%02hhx>", pDataRx[i]);
                        printf("\n");

                        crc = CRC16(&pDataRx[6],6);
                        pDataRx[13] = (crc >> 8) & 0xFF;
                        pDataRx[12] =  crc & 0xFF;

                        printf("RTU Data\n");
                        for(i=6; i<14; i++) printf("[%02hhx]", pDataRx[i]);
                        printf("\n");

                        write(fd, &pDataRx[6], 8);

                        ret = read(fd, &pDataTx[6], sizeof(pDataTx));
                        if(ret>0) {
                            crc_rtu = CRC16(&pDataTx[6],ret-2);
                            crc = (pDataTx[6+ret-2] + (pDataTx[6+ret-1] << 8));
                            printf("%x %x %x %x\n", crc, crc_rtu, pDataTx[6+ret-1], pDataTx[6+ret-2]);

                            printf("RTU Return\n");
                            for(i=6; i<ret+6; i++) printf("(%02hhx)", pDataTx[i]);
                            printf("\n");

                            strncpy(pDataTx, pDataRx, 6);

                            //printf("%d\n",ret);

                            if(crc == crc_rtu) {
                                pDataTx[0] = TR_ID[0];
                                pDataTx[1] = TR_ID[1];

                                sendto(connfd, pDataTx, ret+4, 0, (struct sockaddr *) &cliaddr, sizeof(cliaddr));
                                for(i=0; i<20; i++) printf("*%02hhx*", pDataRx[i]);
                                //printf("%d\n",ret);
                            }
                        }
                    }
                }
                else {
                    close(fd);

                    close(listenfd);		/* close listening socket */
                    close(connfd);		/* parent closes connected socket */
                    exit(0);
                }
            }
        }
    }
}






