# the compiler: gcc for C program, define as g++ for C++
CC = mipsel-openwrt-linux-gcc

# compiler flags:
# -g adds debugging information to the executable file
# -Wall turns on most, but not all, compiler warnings
CFLAGS = -g -Wall

# the build target executable:
TARGET = modbus-gway

OBJ = modbus-gway.o io.o

all: $(TARGET)

$(TARGET): $(OBJ)
        $(CC) $(CFLAGS) $(OBJ) -o $(TARGET)
        rm -f modbus-gway.o io.o

mdbus-gway.o: $(TARGET).c io.h
        $(CC) $(CFLAGS) -c $(TARGET).c

io.o: io.c io.h
        $(CC) $(CFLAGS) -c io.c

clean:
        /bin/rm -f $(TARGET) $(TARGET).o io.o

install:
        scp modbus-gway root@192.168.100.1:/usr/bin/
